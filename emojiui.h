#ifndef EMOJIUI_H
#define EMOJIUI_H

#include <raylib.h>

#define NULL_UIID (UIID){0}

typedef enum
{
	UISTATE_DISABLED = -1,
	UISTATE_DEFAULT = 0,
	UISTATE_FOCUS = 1,
	UISTATE_ACTIVE = 2
} EUIState;

typedef struct UIID
{
	int item;
} UIID;

typedef struct UIContext
{
	UIID hotItem;
	UIID activeItem;

	Vector2 mousePos;
	Vector2 mousePosLast;

	bool leftButtonDown;
	bool leftButtonDownLast;

	bool rightButtonDown;
	bool rightBUttonDownLast;
} UIContext;

UIContext *ictx = 0;

void EUIBindContext(UIContext *ctx)
{
	ictx = ctx;
}

UIID EUICreateID(int index)
{
	return (UIID){ .item = index };
}

float clamp(float d, float min, float max) {
  const float t = d < min ? min : d;
  return t > max ? max : t;
}

void EUIStartFrame()
{
	ictx->mousePosLast = ictx->mousePos;
	ictx->mousePos = GetMousePosition();
	ictx->leftButtonDownLast = ictx->leftButtonDown;
	ictx->leftButtonDown = IsMouseButtonDown(0);
	ictx->rightBUttonDownLast = ictx->rightButtonDown;
	ictx->rightButtonDown = IsMouseButtonDown(2);
	//ictx->hotItem = NULL_UIID;
}

void EUIEndFrame()
{

}

bool PointvRect(Vector2 point, Rectangle rect)
{
	return point.x > rect.x &&
		point.x < rect.x + rect.width &&
		point.y > rect.y &&
		point.y < rect.y + rect.height;
}

bool IsHot(UIID id)
{
	return id.item == ictx->hotItem.item;
}

bool IsActive(UIID id)
{
	return id.item == ictx->activeItem.item;
}

bool EUIButton(UIID id, char *text, Rectangle bounds)
{
	bool result = 0;
	EUIState state = UISTATE_DEFAULT;

	if ((state != UISTATE_DISABLED))
	{
		if (PointvRect(ictx->mousePos, bounds))
		{
			if (ictx->leftButtonDown) state = UISTATE_ACTIVE;
			else state = UISTATE_FOCUS;

			if (!ictx->leftButtonDown && ictx->leftButtonDownLast) result = 1;
		}
	}

	if (state == UISTATE_ACTIVE)
	{
		DrawRectangle(bounds.x, bounds.y, bounds.width, bounds.height, GOLD);
		DrawText(text, bounds.x, bounds.y, 24, BLACK);
	}
	else if (state == UISTATE_FOCUS)
	{
		DrawRectangle(bounds.x, bounds.y, bounds.width, bounds.height, Fade(GRAY, 0.3));
		DrawText(text, bounds.x, bounds.y, 24, GOLD);
	}
	else
	{
		DrawRectangle(bounds.x, bounds.y, bounds.width, bounds.height, Fade(GRAY, 0.2));
		DrawText(text, bounds.x, bounds.y, 24, GOLD);
	}

	return result;
}

void EUISlider(UIID id, char *text, Rectangle bounds, float *value, float min, float max)
{
	EUIState state = UISTATE_DEFAULT;

	if ((state != UISTATE_DISABLED))
	{
		if (PointvRect(ictx->mousePos, bounds))
		{
			if (ictx->leftButtonDown) state = UISTATE_ACTIVE;
			else state = UISTATE_FOCUS;

		}
	}

	if (state == UISTATE_ACTIVE)
	{
		*value = clamp(*value + (GetMouseDelta().x / GetScreenWidth()) * max * bounds.width * 0.01, min, max);
	}
	else
	{
		EnableCursor();
	}

	if (state == UISTATE_ACTIVE)
	{
		DrawRectangle(bounds.x, bounds.y, bounds.width, bounds.height, Fade(GRAY, 0.4));
		DrawRectangle(bounds.x, bounds.y, (*value / max) * bounds.width, bounds.height, WHITE);
	}
	else if (state == UISTATE_FOCUS)
	{
		DrawRectangle(bounds.x, bounds.y, bounds.width, bounds.height, Fade(GRAY, 0.3));
		DrawRectangle(bounds.x, bounds.y, (*value / max) * bounds.width, bounds.height, YELLOW);
	}
	else 
	{
		DrawRectangle(bounds.x, bounds.y, bounds.width, bounds.height, Fade(GRAY, 0.2));
		DrawRectangle(bounds.x, bounds.y, (*value / max) * bounds.width, bounds.height, GOLD);
	}
	
	DrawText(text, bounds.x, bounds.y, 24, BLACK);

}

#endif