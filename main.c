#include <raylib.h>
#include <math.h>
#include <raymath.h>
#include <stdio.h>
#include "emojiui.h"

#define SCREEN_WIDTH 1000
#define SCREEN_HEIGHT 700

#define BUFFER_SIZE 1024
#define SAMPLE_RATE 44100

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#define COLOR GOLD

enum {
	WAVETYPE_SINE,
	WAVETYPE_NOISE,
	WAVETYPE_SAW,
	WAVETYPE_TRIANGLE,
	WAVETYPE_SQUARE,
	WAVETYPE_PULSE
};

typedef struct Oscillator
{
	float phase;
	float phaseStride;
	int wave;

} Oscillator;

void UpdateSignal(float *signal, Oscillator *osc, float volume)
{
	switch (osc->wave)
	{
		case WAVETYPE_NOISE:
			for (int t = 0; t < BUFFER_SIZE; t++)
			{
				osc->phase += osc->phaseStride;
				if (osc->phase >= 1)
					osc->phase -= 1.0f;
			
				signal[t] = volume * (float)GetRandomValue(-255, 255) / 255;
				
			}
			break;
		case WAVETYPE_SINE:
			for (int t = 0; t < BUFFER_SIZE; t++)
			{
				osc->phase += osc->phaseStride;
				if (osc->phase >= 1)
					osc->phase -= 1.0f;
			
				signal[t] = sinf(2.0f * PI * osc->phase) * volume;
				
			}
			break;
		case WAVETYPE_SAW:
			for (int t = 0; t < BUFFER_SIZE; t++)
			{
				osc->phase += osc->phaseStride;
				if (osc->phase >= 1)
					osc->phase -= 1.0f;
			
				signal[t] = ((osc->phase * 2.0f) - 1.0f) * volume;
				
			}
			break;
		case WAVETYPE_TRIANGLE:
			for (int t = 0; t < BUFFER_SIZE; t++)
			{
				osc->phase += osc->phaseStride;
				if (osc->phase >= 1)
					osc->phase -= 1.0f;
			
				if (osc->phase < 0.5f)
					signal[t] = ((osc->phase * 4.0f) - 1.0f) * volume;
				else
					signal[t] = ((osc->phase * -4.0f) + 3.0f) * volume;
				
			}
			break;
		case WAVETYPE_SQUARE:
			for (int t = 0; t < BUFFER_SIZE; t++)
			{
				osc->phase += osc->phaseStride;
				if (osc->phase >= 1)
					osc->phase -= 1.0f;
			
				if (osc->phase < 0.5f)
					signal[t] = (1) * volume;
				else
					signal[t] = (-1) * volume;
				
			}
			break;
		case WAVETYPE_PULSE:
			for (int t = 0; t < BUFFER_SIZE; t++)
			{
				osc->phase += osc->phaseStride;
				if (osc->phase >= 1)
					osc->phase -= 1.0f;
			
				if (osc->phase < 0.2f)
					signal[t] = (1) * volume;
				else
					signal[t] = (-1) * volume;
				
			}
			break;
			
	}
	
}

float GetRMS(float *buffer, int count)
{
	float sum = 0;
	for (int i = 0; i < count; i++)
	{
		sum += buffer[i] * buffer[i];
	}
	sum /= count;
	return sqrtf(sum);

}

int main(void)
{
	InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Emoji Tone Generator v0.2");
	InitAudioDevice();

	UIContext mainUIContext;

	EUIBindContext(&mainUIContext);

	Font mainFont = LoadFont("conthrax-sb.ttf");

	SetAudioStreamBufferSizeDefault(BUFFER_SIZE);
	
	AudioStream synthStream = LoadAudioStream(SAMPLE_RATE, sizeof(float) * 8, 1);
	SetAudioStreamVolume(synthStream, 0.5f);
	PlayAudioStream(synthStream);

	float signal[BUFFER_SIZE];
	float frequency = 440;
	float volume = 1;
	float sampleDuration = 1.0f / SAMPLE_RATE;
	Oscillator synthOsc = {.phase = 0.0f, .phaseStride = frequency * sampleDuration};
	
	int counter = 0;

	SetTargetFPS(600);

	while (!WindowShouldClose())
	{

		if (IsAudioStreamProcessed(synthStream))
		{
			UpdateSignal(signal, &synthOsc, volume);
			UpdateAudioStream(synthStream, signal, BUFFER_SIZE);
			synthOsc.phaseStride = frequency * sampleDuration;
			
		}

		EUIStartFrame();
		BeginDrawing();
		ClearBackground(BLACK);
		DrawLine(0, 350, 1000, 350, Fade(COLOR, 0.4));
		for (int i = 0; i < 10; i++)
		{
			DrawLine(100 * i, 0, 100 * i, SCREEN_HEIGHT, Fade(COLOR, 0.1));
		}
		for (int i = 0; i < BUFFER_SIZE - 1; i++)
		{
			
			//DrawPixel(i, 350 + signal[i] * 200, COLOR);
			DrawLine(i, 350 + signal[i] * 200, i+1, 350 + signal[i+1] * 200, COLOR);
			//frequency += 0.01f * GetFrameTime();
		}

		int decayedHeight;

		DrawRectangle(SCREEN_WIDTH - 30, 0, 30, 
			MAX((int)(GetRMS(signal, BUFFER_SIZE) * SCREEN_HEIGHT), decayedHeight),
			COLOR);
		decayedHeight = MAX((int)(GetRMS(signal, BUFFER_SIZE) * SCREEN_HEIGHT) - 0.1f * GetFrameTime(), (int)(GetRMS(signal, BUFFER_SIZE) * SCREEN_HEIGHT));

		//frequency = 50 + (GetMouseX() / 1000.f) * 1000;
		//volume = (GetMouseY() / 700.f);
		
		DrawTextEx(mainFont, TextFormat("Freq: %ihz", (int)frequency), (Vector2){0, 0}, 32, 1, COLOR);

		DrawTextEx(mainFont, "Emoji Tone Generator", Vector2Subtract((Vector2){SCREEN_WIDTH, SCREEN_HEIGHT}, MeasureTextEx(mainFont, "Emoji Tone Generator", 64, 1)), 64, 1, Fade(COLOR, 0.4));
		


		if (EUIButton(EUICreateID(1), TextFormat("Sine", counter), (Rectangle){720, 20, 100, 50}))
		{
			synthOsc.wave = WAVETYPE_SINE;
		}

		if (EUIButton(EUICreateID(2), TextFormat("Noise", counter), (Rectangle){830, 20, 100, 50}))
		{
			synthOsc.wave = WAVETYPE_NOISE;
		}

		if (EUIButton(EUICreateID(2), "Saw", (Rectangle){610, 20, 100, 50}))
		{
			synthOsc.wave = WAVETYPE_SAW;
		}

		if (EUIButton(EUICreateID(2), "Triangle", (Rectangle){500, 20, 100, 50}))
		{
			synthOsc.wave = WAVETYPE_TRIANGLE;
		}

		if (EUIButton(EUICreateID(2), "Square", (Rectangle){390, 20, 100, 50}))
		{
			synthOsc.wave = WAVETYPE_SQUARE;
		}

		if (EUIButton(EUICreateID(2), "Pulse", (Rectangle){280, 20, 100, 50}))
		{
			synthOsc.wave = WAVETYPE_PULSE;
		}

		EUISlider(EUICreateID(3), "Freq", (Rectangle){20, 600, 400, 40}, &frequency, 0, 2050);
		EUISlider(EUICreateID(3), "Vol", (Rectangle){20, 550, 400, 40}, &volume, 0, 1);

		EndDrawing();
	}



	CloseAudioDevice();
	
	return 0;
}

